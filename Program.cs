﻿using System;



//Esercizio 2: (switch)
//Creare un programma che chiede all'utente di selezionare un opzione tra 4 diverse (Attacca, Difendi, Usa Pozione, Scappa), e stampa una frase diversa in base a quanto esce.
//Il menù di scelta deve essere stampato da un'apposita funzione StampaMenu()

namespace Eserciodueswitch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Decidi se vuoi entrare nel gioco Pincopallo ");

            StampaMenu();
            
        }

        static void StampaMenu()
        {
            Console.WriteLine("Una strega simile a tua madre attacca il tuo castello e decidi di inviare tua suocera");
            Console.WriteLine("Scegli che vorresti fare con quel coso");
            Console.WriteLine("1 : Attacca ");
            Console.WriteLine("2 : Difendi ");
            Console.WriteLine("3 : Mantieni posizione ");
            Console.WriteLine("4 : Scappa ");

            int scelta;
            scelta = Convert.ToInt32(Console.ReadLine());

            switch (scelta)
            {
                case 1:
                    Console.WriteLine("Come previsto è morto miseramente");
                    break;
                case 2:
                    Console.WriteLine("Cercando di difendersi si è ficcato la spada nel deretano,morendo");
                    break;
                case 3:
                    Console.WriteLine("Il drago l'ha raggiunto ed è morto bruciacchiato ");
                    break;
                case 4:
                    Console.WriteLine("Fuggendo non ha visto il burrone");
                    break;
                default:
                    Console.WriteLine("Saltando in cielo raggiunge la strega e sguaina il nulla, muore male. Ora premi un numero da 1 a 4 perfavore.");
                    break;
            }
        }
    }
}
